package vista;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public class MainApp {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		// Desactivar log de mariadb
		((Logger) LoggerFactory.getLogger("org.mariadb.jdbc")).setLevel(Level.INFO);
		App.main(args);
	}
}
