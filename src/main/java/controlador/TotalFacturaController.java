package controlador;

import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import controladorbd.ConexionBD;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

public class TotalFacturaController implements Initializable {
	@FXML
	ComboBox<Integer> cbFacturas;
	@FXML
	Label lblTotal;

	CallableStatement cstmt;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			Connection con = ConexionBD.getConexion();

			// Preparamos la llamada al procedimiento (solo se hace una vez)
			cstmt = con.prepareCall("{call empresa.total_factura(?,?)}");
			// Registramos_el_parametro_de_salida_parametro_2;
			cstmt.registerOutParameter(2, java.sql.Types.DOUBLE);

			// Cargamos listado de facturas en el combo
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select id from empresa.factura");
			while (rs.next()) {
				// System.out.print(rs.getInt(1)+ " ");
				cbFacturas.getItems().add(rs.getInt(1));
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			Platform.exit();
		}

	}

	@FXML
	private void calculaTotal() {
		try {
			// Damos_valor al id de factura (parametro 1);
			cstmt.setInt(1, cbFacturas.getSelectionModel().getSelectedItem());
			// Ejecutamos el callable statement;
			cstmt.execute();
			lblTotal.setText(String.valueOf(cstmt.getDouble(2)) + " €");

		} catch (SQLException e) {
			System.err.println("Error calculando total..." + e.getMessage());
		}
	}

}
